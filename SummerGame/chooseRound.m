//
//  chooseRound.m
//  SummerGame
//
//  Created by xiayanan on 12-7-14.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "chooseRound.h"
#import "HelloWorldLayer.h"
#import "firstRound.h"
#import "secondRound.h"
#import "thirdRound.h"
#import "fourthRound.h"


@implementation chooseRound



+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	chooseRound *layer = [chooseRound node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}





// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
		
        CCSprite *back = [CCSprite spriteWithFile:@"photo.jpg"];
        [back setPosition:CGPointMake(240, 160)];
        [back setScaleX:0.96f];
        [back setScaleY:0.8533f];
        [self addChild:back];
        
				// ask director the the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
       		
        CCMenuItem *back1=[CCMenuItemFont itemFromString:@"BACK" target:self selector:@selector(backNewgame:)];
        [back1 setPosition:CGPointMake(size.width/4,-size.height/3)];
        
        CCMenuItemImage *firstRound = [CCMenuItemImage itemFromNormalImage:@"1.png" selectedImage:@"1.png" target:self selector:@selector(firstRoundMenuScene:)];
        [firstRound setPosition:CGPointMake(-size.width/3,size.height/6)];
        
        CCMenuItemImage *secondRound = [CCMenuItemImage itemFromNormalImage:@"2.png" selectedImage:@"2.png" target:self selector:@selector(secondRoundMenuScene:)];
        [secondRound setPosition:CGPointMake(-60,size.height/6)];
        
        CCMenuItemImage *thirdRound = [CCMenuItemImage itemFromNormalImage:@"3.png" selectedImage:@"3.png" target:self selector:@selector(thirdRoundMenuScene:)];
        [thirdRound setPosition:CGPointMake(60,size.height/6)];
        
        CCMenuItemImage *fourthRound = [CCMenuItemImage itemFromNormalImage:@"4.png" selectedImage:@"4.png" target:self selector:@selector(fourthRoundMenuScene:)];
        [fourthRound setPosition:CGPointMake(size.width/3,size.height/6)];
        
        
        CCMenu *menu=[CCMenu menuWithItems:back1,firstRound,secondRound,thirdRound,fourthRound,nil];
        [self addChild:menu];
  
	}
	return self;
}


- (void) backNewgame : (id) sender
{
    //CCScene *back = [CCScene node];
    //[back addChild:[HelloWorldLayer node]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionJumpZoom transitionWithDuration:1.5f scene:[HelloWorldLayer scene]]];
}

- (void) firstRoundMenuScene : (id) sender
{
    CCScene *first = [CCScene node];
    [first addChild:[firstRound node]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionCrossFade transitionWithDuration:1.5f scene:first]];
   
    
}

- (void) secondRoundMenuScene : (id) sender
{
    CCScene *second = [CCScene node];
    [second addChild:[secondRound node]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionCrossFade transitionWithDuration:1.5f scene:second]];
    
    
}

- (void) thirdRoundMenuScene : (id) sender
{
    CCScene *third = [CCScene node];
    [third addChild:[thirdRound node]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionCrossFade transitionWithDuration:1.5f scene:third]];
    
    
}

- (void) fourthRoundMenuScene : (id) sender
{
    CCScene *fourth = [CCScene node];
    [fourth addChild:[fourthRound node]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionCrossFade transitionWithDuration:1.5f scene:fourth]];
    
    
}





@end
