//
//  gameWork.h
//  SummerGame
//
//  Created by Lazy.TJU on 12-7-20.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface gameWork : CCLayer {
    
}

+ (BOOL) isInFoodArea: (CGPoint)currentPoint
                 food: (CCSprite *) foodSprite;
+ (BOOL) isInWhirlpoolArea:(CGPoint)currentPoint
                   barrier:(CCSprite *)barrierSprite
                    action:(CCSprite *)actionSprite;
+ (BOOL) isInControlPointArea: (CGPoint) point
                  spritePoint:(CGPoint)controlPointSpritePoint;
+ (void) transToSummary:(CCSprite *)summaryImage
                barrier:(CCSprite *)barrierSprite
                 number:(int)remainFood
                  array:(CCArray *)food
                   menu:(CCMenuItem *)nextRound;

@end

