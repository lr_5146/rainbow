//
//  help.m
//  SummerGame
//
//  Created by xiayanan on 12-7-14.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "help.h"
#import "HelloWorldLayer.h"

@implementation help

// on "init" you need to initialize your instance
- (id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
		
        //CGSize size = [[CCDirector sharedDirector] winSize];
        //create background
        CCSprite *back = [CCSprite spriteWithFile:@"default.png"];
        [back setPosition:CGPointMake(240, 160)];
        [back setScaleX:1.5f];
        [back setScaleY:0.667f];
        [self addChild:back];
        
		//create a back button
        CCMenuItemImage *backMenu = [CCMenuItemImage itemFromNormalImage:@"icon.png" selectedImage:@"icon.png" target:self selector:@selector(backMenuScene:)];
        [backMenu setPosition:CGPointMake(0 , 0)];
        CCMenu *menu = [CCMenu menuWithItems:backMenu, nil];
        [self addChild:menu];
        
	}
	return self;
}


- (void) backMenuScene : (id) sender
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionCrossFade transitionWithDuration:1.5f scene:[HelloWorldLayer scene]]];
    //CCScene *back = [CCScene node];
    //[back addChild:[HelloWorldLayer node]];
    //[[CCDirector sharedDirector] replaceScene:[CCTransitionCrossFade transitionWithDuration:1.5f scene:back]];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

@end
