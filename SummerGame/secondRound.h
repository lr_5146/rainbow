//
//  secondRound.h
//  SummerGame
//
//  Created by xiayanan on 12-7-14.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface secondRound : CCLayer {
    
    CGSize size;
    CGPoint controlPoint;
    CGPoint touchPointStart;
    CGPoint touchPointEnd;
    CGPoint controlPointSpritePoint;
    
    CCSprite *controlSprite;
    CCSprite *actionSprite;
    CCSprite *summaryImage;
    CCSprite *barrierSprite;
    CCMenuItem *nextRound;
    
    ccTime time;
    ccTime actionTime;
    CCArray *food;
    BOOL runOnce;//hehe
    int remainFood;//haha
}

- (void) initGameScene;
- (void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;

@end
