//
//  secondRound.m
//  SummerGame
//
//  Created by xiayanan on 12-7-14.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "secondRound.h"
#import "chooseRound.h"
#import "thirdRound.h"
#import "SimpleAudioEngine.h"
#import "gameWork.h"

@implementation secondRound

-(id) init
{
    if((self=[super init]))
    {
        self.isTouchEnabled = YES;
        [self initGameScene];
    }
    return self;
}




- (void) initGameScene
{
    runOnce = YES;
    CCSprite *back=[ CCSprite spriteWithFile:@"fi.jpg"];
    [back setPosition:CGPointMake(240,160)]; 
    [back setScaleX:0.25f];
    [back setScaleY:0.267f];
    //[self addChild:back z:0];
    size=[[CCDirector sharedDirector] winSize];
    
    CCSprite *startSprite=[CCSprite spriteWithFile:@"Icon.png"];
    [startSprite setPosition:CGPointMake(size.width/12, size.height/2)];
    [startSprite setScale:0.5];
    actionSprite = startSprite;
    [self addChild:actionSprite z:0];
    CCSprite *end=[CCSprite spriteWithFile:@"y_point.png"];
    [end setPosition:CGPointMake(size.width*11/12, size.height/2)];
    [self addChild:end z:0];
    
    CCMenuItem *reStart = [CCMenuItemImage itemFromNormalImage:@"b_point.png" selectedImage:@"b_point.png" target:self selector:@selector(restartGame:)];
    [reStart setPosition:CGPointMake(-size.width / 2.3, -size.height / 3)];
    CCMenuItem *runAction = [CCMenuItemFont itemFromString:@"RUN" target:self selector:@selector(runSpriteAction:)];
    [runAction setPosition:CGPointMake(-size.width / 4, - size.height /3 )];
    CCMenuItem *backStartgame=[CCMenuItemFont itemFromString:@"BACK" target:self selector:@selector(backNewgame:)];
    [backStartgame setPosition:CGPointMake(size.width/4, -size.height/3)];
    nextRound = [CCMenuItemImage itemFromNormalImage:@"y_point.png" selectedImage:@"y_point.png" target:self selector:@selector(transToNextRound)];
    [nextRound setPosition:CGPointMake(size.width / 2.3, -size.height / 3)];
    [nextRound setIsEnabled:NO];
    [nextRound setVisible:NO];
    CCMenu *menu=[CCMenu menuWithItems:reStart, runAction, backStartgame, nextRound,nil];
    [self addChild:menu z:1];
    
    
    controlPoint.x = size.width / 2;
    controlPoint.y = size.height / 2;
    CCSprite *controlPointSprite = [CCSprite spriteWithFile:@"icon.png"];
    [controlPointSprite setAnchorPoint:CGPointMake(0.5, 0.5)];
    [controlPointSprite setScale:0.2f];
    [controlPointSprite setPosition:CGPointMake(controlPoint.x, controlPoint.y)];
    controlPointSpritePoint.x = controlPoint.x;
    controlPointSpritePoint.y = controlPoint.y;
    controlSprite = controlPointSprite;
    [self addChild:controlSprite z:0];
    [self draw];
    
    CCLabelTTF *lbScore=[CCLabelTTF labelWithString:@"time: 0" fontName:@"Marker Felt" fontSize:32];
    lbScore.scale=0.6;
    [self addChild:lbScore z:2 tag:3];
    [lbScore setPosition:CGPointMake(size.width/11, size.height*11/12)];
    [self schedule:@selector(step:) interval:0.01];
    lbScore.color=ccc3(12, 120, 160);
    
    summaryImage = [CCSprite spriteWithFile:@"fi.jpg"];
    [summaryImage setScaleX:0.25f];
    [summaryImage setScaleY:0.267f];
    [summaryImage setPosition:CGPointMake(240, 160)];
    [self addChild:summaryImage z:0];
    [summaryImage setVisible:NO];
    
    barrierSprite = [CCSprite spriteWithFile:@"whirlpool.png"];
    [barrierSprite setPosition:CGPointMake(size.width / 3, size.height * 3/4 )];
    [self addChild:barrierSprite z:1];
    
    food = [[CCArray alloc] initWithCapacity:3];
    CCSprite *food1=[CCSprite spriteWithFile:@"red_flower.png"];
    [food1 setPosition:CGPointMake(size.width*1/24, size.height*3/4)];
    [self addChild:food1];
    [food addObject:food1];
    CCSprite *food2=[CCSprite spriteWithFile:@"blue_flower.png"];
    [food2 setPosition:CGPointMake(size.width/6, size.height*7/8)];
    [self addChild:food2];
    [food addObject:food2];
    CCSprite *food3=[CCSprite spriteWithFile:@"green_flower.png"];
    [food3 setPosition:CGPointMake(size.width*17/24, size.height*3/5)];
    [self addChild:food3];
    [food addObject:food3];
    remainFood = 3;
    
    for(NSUInteger i = 0; i < 3;i++)
    {
        [[food objectAtIndex:i] setVisible:YES];
    }
}


- (void) draw
{
    glLineWidth(10);
    glColor4f(0, 255, 187, 0);
    ccDrawQuadBezier(CGPointMake(size.width/12, size.height/2), controlPoint, CGPointMake(size.width*11/12, size.height/2),30);
}

- (void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:[touch view]];
    CGPoint convertedLocaiton =[[CCDirector sharedDirector] convertToGL:location];
    touchPointStart = convertedLocaiton;
    if(![gameWork isInControlPointArea:touchPointStart spritePoint:controlPointSpritePoint])
    {
        return;
    }
}

- (void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:[touch view]];
    CGPoint convertedLocaiton =[[CCDirector sharedDirector] convertToGL:location];
    touchPointEnd = convertedLocaiton;
    if(![gameWork isInControlPointArea:touchPointEnd spritePoint:controlPointSpritePoint])
    {
        return;
    }
    controlPoint.x = 2 * touchPointEnd.x - 0.5 * size.width;
    controlPoint.y = 2 * touchPointEnd.y - 0.5 * size.height;
    [controlSprite setPosition:touchPointEnd];
    controlPointSpritePoint = touchPointEnd;
    [self draw];
}

- (void) restartGame:(id)sender
{
    CCScene *newScene = [CCScene node];
    [newScene addChild:[secondRound node]];
    [[CCDirector sharedDirector] replaceScene:newScene];
}

- (void) transToNextRound
{
    CCScene *newScene = [CCScene node];
    [newScene addChild:[thirdRound node]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionRotoZoom transitionWithDuration:2.0f scene:newScene]];
}

- (void) runSpriteAction: (id)sender
{
    if(!runOnce)
    {
        return;
    }
    ccBezierConfig bezier;
	bezier.controlPoint_1 = CGPointMake(size.width/12, size.height/2);
    bezier.controlPoint_2 = controlPoint;
	bezier.endPosition = ccp(size.width*11/12, size.height/2);
	id bezierForward = [CCBezierTo actionWithDuration:3 bezier:bezier];
	CCAction *seq = [CCSequence actions: bezierForward, nil];
    [actionSprite runAction:seq];
    [seq setTag:10];
    [self schedule:@selector(judgePoint:) interval:0.01];
}


- (void) judgePoint:(ccTime)dt
{
    if(!runOnce)
    {
        return;
    }
    if(actionTime > 3 && runOnce)
    {
        [gameWork transToSummary:summaryImage barrier:barrierSprite number:remainFood array:food menu:nextRound];
        runOnce = NO;
    }
    actionTime += dt;
    CGPoint currentPoint;
    CCSprite *currentSprite;
    currentPoint = [actionSprite position];    
    BOOL isFoodVisible = NO;
    if([gameWork isInWhirlpoolArea:currentPoint barrier:barrierSprite action:actionSprite])
    {
        [gameWork transToSummary:summaryImage barrier:barrierSprite number:remainFood array:food menu:nextRound];
    }
    for(NSUInteger i = 0;i < 3;i++)
    {
        currentSprite = [food objectAtIndex:i];
        isFoodVisible = [currentSprite visible];
        //isFoodVisible = [[food objectAtIndex:i] isVisible];
        if([gameWork isInFoodArea:currentPoint food:[food objectAtIndex:i]] && isFoodVisible != NO)
        {
            [[food objectAtIndex:i] setVisible:NO];
            remainFood--;
            [[SimpleAudioEngine sharedEngine] preloadEffect:@"eatFood.mp3"];

        }
    }
}


-(void) step:(ccTime)dt
{
    if (actionTime > 3) {
        return;
    }
    time+= dt;
    NSString *string=[NSString stringWithFormat:@"time: %0.2f",(float)time];
    CCLabelTTF *label1=(CCLabelTTF *)[self getChildByTag:3];
    [label1 setString:string];
}


-(void)backNewgame:(id)sender
{
   // [[CCDirector sharedDirector] replaceScene:[CCTransitionFadeUp transitionWithDuration:1.5f scene:[chooseRound scene]]];
    [[CCDirector sharedDirector] replaceScene:[chooseRound scene]];
}

@end
