//
//  aboutMonster.m
//  SummerGame
//
//  Created by xiayanan on 12-7-14.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "aboutMonster.h"
#import "HelloWorldLayer.h"


@implementation aboutMonster

-(id) init
{
    if((self=[super init]))
    {
        CCSprite *back=[ CCSprite spriteWithFile:@"default.png"];
        [back setPosition:CGPointMake(240,160)]; 
        [back setScaleX:1.5f];
        [back setScaleY:0.667f];
        [self addChild:back];
    
    
    CGSize size=[[CCDirector sharedDirector] winSize];
    
        CCMenuItem *backStartgame=[CCMenuItemFont itemFromString:@"BACK" target:self selector:@selector(backNewgame:)];
    [backStartgame setPosition:CGPointMake(size.width/4, -size.height/3)];
    CCMenu *menu=[CCMenu menuWithItems:backStartgame, nil];
    [self addChild:menu];
                               
    }
    return self;
}

-(void)backNewgame:(id)sender
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFadeDown transitionWithDuration:1.5f scene:[HelloWorldLayer scene]]];
}
@end
