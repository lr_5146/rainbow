//
//  HelloWorldLayer.m
//  SummerGame
//
//  Created by Lazy.TJU on 12-7-14.
//  Copyright __MyCompanyName__ 2012年. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"
#import "chooseRound.h"
#import "help.h"
#import "aboutMonster.h"
#import "SimpleAudioEngine.h"

// HelloWorldLayer implementation
@implementation HelloWorldLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
		
        CCSprite *back = [CCSprite spriteWithFile:@"default.png"];
        [back setPosition:CGPointMake(240, 160)];
        [back setScaleX:1.5f];
        [back setScaleY:0.667f];
        //[self addChild:back];
        
		// create and initialize a Label
		CCLabelTTF *label = [CCLabelTTF labelWithString:@"RainBow Star" fontName:@"Marker Felt" fontSize:32];

		// ask director the the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
	
		// position the label on the center of the screen
		label.position =  ccp( size.width /2 , size.height/2+60
                              );
		
		// add the label as a child to this Layer
		[self addChild: label];
        
        
        CCMenuItem *chooseRound=[CCMenuItemFont itemFromString:@"NEWGAME" target:self selector:@selector(chooseRoundScene:)];
        CCMenuItem *help = [CCMenuItemFont itemFromString:@"HELP" target:self selector:@selector(helpScene:)];
        CCMenuItem *monsterView = [CCMenuItemFont itemFromString:@"MONSTER VIEW" target:self selector:@selector(monsterViewScene:)];
        [chooseRound setPosition:CGPointMake(-size.width / 6, -size.height / 8)];
        [help setPosition:CGPointMake(-size.width / 6 + size.height / 2, - size.height / 8)];
        [monsterView setPosition:CGPointMake(-size.width / 6 + size.width / 5, - size.height / 3)];
        CCMenu *menu=[CCMenu menuWithItems:chooseRound, help, monsterView, nil];
        [self addChild:menu];
        
        
        ///////////////////////////////////////////////////////////////////////////
        CCSprite * sprite=[CCSprite spriteWithFile:@"stars.jpg"];
        sprite.anchorPoint=CGPointMake(0, 0);
        [sprite setScale:0.2f];
        [self addChild:sprite];
        CCMoveTo*move=[CCMoveTo actionWithDuration:3  position:CGPointMake(440, 280)];
        CCRepeatForever*repeat=[CCRepeatForever actionWithAction:move];
        
        CCLabelTTF* label11=[CCLabelTTF labelWithString:@"hello" fontName:@"applegothic" fontSize:32];
        [self addChild:label11]; 
        
         [sprite runAction:repeat];
        
        label11.position =  ccp( size.width /6 , 300
                              );
        
        CCTintTo* tint1=[CCTintTo actionWithDuration:4 red:255 green:0 blue:0];
        CCTintTo* tint2=[CCTintTo actionWithDuration:4 red:0 green:0 blue:255];
        CCTintTo* tint3=[CCTintTo actionWithDuration:4 red:0 green:255 blue:0];
        CCSequence *sequence=[CCSequence actions:tint1,tint2,tint3, nil];
        CCRepeatForever*repeat11=[CCRepeatForever actionWithAction:sequence];
        [label runAction:repeat11];
        
        CCMenuItemFont* toggleon = [CCMenuItemFont itemFromString:@"music OFF"];
        CCMenuItemFont* toggleoff = [CCMenuItemFont itemFromString:@"music ON"];
        CCMenuItemToggle* item3=[CCMenuItemToggle itemWithTarget:self selector:@selector(choosemusic:) items:toggleon,toggleoff, nil];
       CCMenu *menu1=[CCMenu menuWithItems:item3, nil];
        [self addChild:menu1];
                
        
                
	}
	return self;
}


-(void) choosemusic :(id)sender
{
    //background music
    if(isMusicPlay == NO)
    {
        [[SimpleAudioEngine sharedEngine]playBackgroundMusic:@"fruit.mp3" loop:YES];
        isMusicPlay = YES;
    }
    else {
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        isMusicPlay = NO;
    }
    //音效[[SimpleAudioEngine sharedEngine]playEffect:@"alien-sfx.caf"];
}




- (void) chooseRoundScene : (id) sender
{
    CCScene *choose = [CCScene node];
    [choose addChild:[chooseRound node]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionJumpZoom transitionWithDuration:1.5f scene:choose]];
}

- (void) helpScene : (id) sender
{
    CCScene *helpScene = [CCScene node];
    [helpScene addChild:[help node]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionShrinkGrow transitionWithDuration:1.5f scene:helpScene]];
}

- (void) monsterViewScene : (id) sender
{
    CCScene *monsterView = [CCScene node];
    [monsterView addChild:[aboutMonster node]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionZoomFlipAngular transitionWithDuration:1.5f scene:monsterView]];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
