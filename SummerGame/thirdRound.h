//
//  thirdRound.h
//  SummerGame
//
//  Created by xiayanan on 12-7-14.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface thirdRound : CCLayer {
    
    CGSize size;
    CGPoint controlPointOne;
    CGPoint controlPointTwo;
    CGPoint touchPointStart;
    CGPoint touchPointEnd;
    CGPoint controlPointOneSpritePoint;
    CGPoint controlPointTwoSpritePoint;
    
    CCSprite *controlSpriteOne;
    CCSprite *controlSpriteTwo;
    CCSprite *actionSprite;
    CCSprite *summaryImage;
    CCSprite *touchSprite;
    CCMenuItem *nextRound;
    
    ccTime time;
    ccTime actionTime;
    CCArray *food;
    BOOL runOnce;
    int remainFood;
}

- (void) initGameScene;
- (void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
@end
