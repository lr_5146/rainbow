//
//  gameWork.m
//  SummerGame
//
//  Created by Lazy.TJU on 12-7-20.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import "gameWork.h"
#import "SimpleAudioEngine.h"

@implementation gameWork

-(id) init
{
    
    if((self=[super init]))
    {
    }
    return self;
}

+ (BOOL) isInFoodArea: (CGPoint)currentPoint
                 food: (CCSprite *) foodSprite
{
    CGPoint foodPoint = [foodSprite position];
    if((currentPoint.x >= (foodPoint.x - 24)) &&
       (currentPoint.x <= (foodPoint.x + 24)) &&
       (currentPoint.y >= (foodPoint.y - 24)) &&
       (currentPoint.y <= (foodPoint.y + 24)))
    {
        return YES;
    }
    else {
        return NO;
    }
}

+ (BOOL) isInWhirlpoolArea:(CGPoint)currentPoint
        barrier:(CCSprite *)barrierSprite
        action:(CCSprite *)actionSprite
{
    CGPoint barrierPoint = [barrierSprite position];
    if((currentPoint.x >= (barrierPoint.x - 44)) &&
       (currentPoint.x <= (barrierPoint.x + 44)) &&
       (currentPoint.y >= (barrierPoint.y - 44)) &&
       (currentPoint.y <= (barrierPoint.y + 44)))
    {
        [actionSprite stopActionByTag:10];
        return YES;
    }
    else {
        return NO;
    }
}

+ (BOOL) isInControlPointArea: (CGPoint) point
                  spritePoint:(CGPoint)controlPointSpritePoint
{
    CGPoint controlPointSprite = controlPointSpritePoint;
    if ((point.x >= (controlPointSprite.x - 11.4)) && 
        (point.x <= (controlPointSprite.x + 11.4)) &&
        (point.y >= (controlPointSprite.y - 11.4)) &&
        (point.y <= (controlPointSprite.y + 11.4)))
    {
        return YES;
    }
    else {
        return NO;
    }
}

+ (void) transToSummary:(CCSprite *)summaryImage
                barrier:(CCSprite *)barrierSprite
                 number:(int)remainFood
                  array:(CCArray *)food
                   menu:(CCMenuItem *)nextRound;
{
    [summaryImage setVisible:YES];
    BOOL isFoodStillVisible;
    [barrierSprite setVisible:NO];
    NSUInteger arrayQuantity = [food count];
    if(remainFood > 0)
    {
        
        for (NSUInteger i = 0; i < arrayQuantity; i++) {
            isFoodStillVisible = [[food objectAtIndex:i] visible];
            if(isFoodStillVisible)
                [[food objectAtIndex:i] setVisible:NO];
        }
    }
    else {
        [nextRound setVisible:YES];
        [nextRound setIsEnabled:YES];
    }
}


@end
